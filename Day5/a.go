package main

import (
	"bufio"
)

func runA(scn *bufio.Scanner) int {
	ventMap := VentMap{}

	for scn.Scan() {
		input := scn.Text()
		x1, y1, x2, y2 := ParseInput(input)

		ventMap.FillCoordinates(x1, y1, x2, y2, false)
	}

	return ventMap.CountOverlap()
}