package main

import "math"

type VentMap struct {
	// Assumption that we never exceed the 1000,1000 on our map.
	// More future proofing would be to dynamically allow the passing
	// of these row and column sizes, however that didn't seem required
	// for this challenge. Therefore, this has been left out for the sake
	// of development speed.
	ventMap [1000][1000]int
}

func (v *VentMap) FillCoordinates(x1, y1, x2, y2 int, includeDiagonals bool) {
	if x1 == x2 {
		from := int(math.Min(float64(y1), float64(y2)))
		to := int(math.Max(float64(y1), float64(y2)))

		for i := from; i <= to; i++ {
			v.ventMap[i][x1]++
		}
	} else if y1 == y2 {
		from := int(math.Min(float64(x1), float64(x2)))
		to := int(math.Max(float64(x1), float64(x2)))

		for i := from; i <= to; i++ {
			v.ventMap[y1][i]++
		}
	} else if includeDiagonals && math.Abs(float64(x1 - x2)) == math.Abs(float64(y1 - y2)) {
		xStep := 1
		if x1 > x2 {
			xStep = -1
		}

		yStep := 1
		if y1 > y2 {
			yStep = -1
		}

		requiredSteps := int(math.Abs(float64(x1 - x2)))
		for i := 0; i <= requiredSteps; i++ {
			v.ventMap[y1 + (i * yStep)][x1 + (i * xStep)]++
		}
	}
}

func (v *VentMap) CountOverlap() int {
	count := 0

	for i := 0; i < len(v.ventMap); i++ {
		for j := 0; j < len(v.ventMap[i]); j++ {
			if v.ventMap[i][j] > 1 {
				count++
			}
		}
	}

	return count
}