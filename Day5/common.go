package main

import (
	"strconv"
	"strings"
)

func ParseInput(input string) (int, int, int, int) {
	commands := strings.Split(input, " -> ")
	firstCommands := strings.Split(commands[0], ",")
	secondCommands := strings.Split(commands[1], ",")
	x1String, y1String, x2String, y2String := firstCommands[0], firstCommands[1], secondCommands[0], secondCommands[1]
	x1, _ := strconv.Atoi(x1String)
	y1, _ := strconv.Atoi(y1String)
	x2, _ := strconv.Atoi(x2String)
	y2, _ := strconv.Atoi(y2String)

	return x1, y1, x2, y2
}
