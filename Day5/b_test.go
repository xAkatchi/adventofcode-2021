package main

import (
	"AdventOfCode2021"
	"testing"
)

func TestRunB(t *testing.T) {
	tester := AdventOfCode2021.ChallengeTester{
		DayNr:      5,
		Part:       'B',
		TestMethod: runB,
	}

	tester.TestChallenge(t, []AdventOfCode2021.TestData{
		// Test case from the Advent of Code website
		{Input: "0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2", ExpectedOutput: 12},
	})
}
