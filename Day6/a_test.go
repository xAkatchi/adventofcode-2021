package main

import (
	"AdventOfCode2021"
	"testing"
)

func TestRunA(t *testing.T) {
	tester := AdventOfCode2021.ChallengeTester{
		DayNr:      6,
		Part:       'A',
		TestMethod: runA,
	}

	tester.TestChallenge(t, []AdventOfCode2021.TestData{
		// Test case from the Advent of Code website
		{Input: "3,4,3,1,2", ExpectedOutput: 5934},
	})
}
