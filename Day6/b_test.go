package main

import (
	"AdventOfCode2021"
	"testing"
)

func TestRunB(t *testing.T) {
	tester := AdventOfCode2021.ChallengeTester{
		DayNr:      6,
		Part:       'B',
		TestMethod: runB,
	}

	tester.TestChallenge(t, []AdventOfCode2021.TestData{
		// Test case from the Advent of Code website
		{Input: "3,4,3,1,2", ExpectedOutput: 26984457539},
	})
}
