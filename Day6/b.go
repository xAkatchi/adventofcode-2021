package main

import (
	"bufio"
	"strconv"
	"strings"
)

func runB(scn *bufio.Scanner) int {
	scn.Scan()
	input := strings.Split(scn.Text(), ",")
	fishes := make(map[int]int)

	for _, internalTimer := range input {
		timerAsInt, _ := strconv.Atoi(internalTimer)
		fishes[timerAsInt]++
	}

	return Simulate(fishes, 256)
}

