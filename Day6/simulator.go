package main

// Simulate simulates the breeding process of the given initialState of fishes
// where the key is the age of the fish and the value is the amount of fishes in that group.
// It will run this simulation for the given amount of days, returning the total amount of fishes
// in all age groups.
func Simulate(initialState map[int]int, days int) int {
	for i := 0; i < days; i++ {
		// Create a temporary map to hold the result of the aged fishes.
		// This is done to simplify the aging code at the cost of more memory.
		agedFishes := make(map[int]int)

		// Special conditions, if we're at 0, we will move all the fishes
		// back to the start position (nr 6) and create new initialState (babies)
		// at position 8.
		agedFishes[6] = initialState[0]
		agedFishes[8] = initialState[0]

		// And for the rest we will just follow the normal 'aging' behavior.
		agedFishes[0] = initialState[1]
		agedFishes[1] = initialState[2]
		agedFishes[2] = initialState[3]
		agedFishes[3] = initialState[4]
		agedFishes[4] = initialState[5]
		agedFishes[5] = initialState[6]
		// Note: += here since we've added all the fishes from pos 0 here as well.
		agedFishes[6] += initialState[7]
		agedFishes[7] = initialState[8]

		initialState = agedFishes
	}

	fishCount := 0
	for _, fishCountByAge := range initialState {
		fishCount += fishCountByAge
	}

	return fishCount
}