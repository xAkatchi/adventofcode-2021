package main

import "AdventOfCode2021"

func main() {
	executor := AdventOfCode2021.ChallengeExecutor{
		DayNumber: 4,
		RunA:      runA,
		RunB:      runB,
	}
	executor.Execute()
}
