package main

import (
	"bufio"
	"strconv"
	"strings"
)

func runB(scn *bufio.Scanner) int {
	bingoCounter := 0

	scn.Scan()
	bingoInput := strings.Split(scn.Text(), ",")
	scn.Scan()

	bingoCards := CreateBingoCards(scn)

	for _, strNumber := range bingoInput {
		number, _ := strconv.Atoi(strNumber)

		for _, card := range bingoCards {
			if card.HasBingo() {
				continue
			}

			card.MarkNumber(number)

			if card.HasBingo() {
				bingoCounter++

				if bingoCounter == len(bingoCards) {
					return card.SumOfUnmarkedNumbers() * number
				}
			}
		}
	}

	return -1
}