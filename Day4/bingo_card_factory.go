package main

import (
	"bufio"
	"strconv"
	"strings"
)

func CreateBingoCards(scn *bufio.Scanner) []*BingoCard {
	var bingoCards []*BingoCard

	var cardInput [][]int
	for scn.Scan() {
		line := scn.Text()

		if line == "" {
			bingoCards = append(bingoCards, NewBingoCard(cardInput))
			cardInput = [][]int{}
			continue
		}

		splitLine := filterEmpty(strings.Split(line, " "))
		inputLine := make([]int, len(splitLine))

		for i, s := range splitLine {
			inputLine[i], _ = strconv.Atoi(s)
		}

		cardInput = append(cardInput, inputLine)
	}
	return append(bingoCards, NewBingoCard(cardInput))
}

func filterEmpty(s []string) []string {
	var r []string

	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}

	return r
}
