package main

import (
	"bufio"
	"strconv"
	"strings"
)

func runA(scn *bufio.Scanner) int {
	scn.Scan()
	bingoInput := strings.Split(scn.Text(), ",")
	scn.Scan()

	bingoCards := CreateBingoCards(scn)

	for _, strNumber := range bingoInput {
		number, _ := strconv.Atoi(strNumber)

		for _, card := range bingoCards {
			card.MarkNumber(number)

			if card.HasBingo() {
				return card.SumOfUnmarkedNumbers() * number
			}
		}
	}

	return -1
}