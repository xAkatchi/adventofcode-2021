package main

type BingoCard struct {
	fields [][]int
}

const MarkedNr = -999

func NewBingoCard(fields [][]int) *BingoCard {
	return &BingoCard{
		fields: fields,
	}
}

func (c *BingoCard) MarkNumber(nr int) {
	for i, _ := range c.fields {
		for j, _ := range c.fields[i] {
			if c.fields[i][j] == nr {
				c.fields[i][j] = MarkedNr
			}
		}
	}
}

func (c *BingoCard) HasBingo() bool {
	return c.hasHorizontalBingo() || c.hasVerticalBingo()
}

func (c *BingoCard) hasHorizontalBingo() bool {
	for i, _ := range c.fields {
		allMarked := true

		for j, _ := range c.fields[i] {
			if c.fields[i][j] != MarkedNr {
				allMarked = false
			}
		}

		if allMarked {
			return true
		}
	}

	return false
}

func (c *BingoCard) hasVerticalBingo() bool {
	for i, _ := range c.fields {
		allMarked := true


		for j := 0; j < len(c.fields[i]); j++ {
			if c.fields[j][i] != MarkedNr {
				allMarked = false
			}
		}

		if allMarked {
			return true
		}
	}

	return false
}

func (c *BingoCard) SumOfUnmarkedNumbers() int {
	var sum int

	for i, _ := range c.fields {
		for j, _ := range c.fields[i] {
			if c.fields[i][j] != MarkedNr {
				sum += c.fields[i][j]
			}
		}
	}

	return sum
}