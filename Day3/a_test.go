package main

import (
	"AdventOfCode2021"
	"testing"
)

func TestRunA(t *testing.T) {
	tester := AdventOfCode2021.ChallengeTester{
		DayNr:      3,
		Part:       'A',
		TestMethod: runA,
	}

	tester.TestChallenge(t, []AdventOfCode2021.TestData{
		// Test case from the Advent of Code website
		{Input: "00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010", ExpectedOutput: 198},
	})
}
