package main

import (
	"bufio"
	"strconv"
)

type BitCriteria string

const (
	OXYGEN BitCriteria = "Oxygen"
	CO2 BitCriteria = "CO2"
)

func runB(scn *bufio.Scanner) int {
	var input []string

	// Since we're iterating twice over the input, we will just create an array
	// containing the input. This is easier to work with than streaming the input
	// multiple times.
	for scn.Scan() {
		input = append(input, scn.Text())
	}

	binaryOxygenRating := determineRating(OXYGEN, input, 0)
	binaryCo2Rating := determineRating(CO2, input, 0)

	oxygenGeneratorRating, _ := strconv.ParseInt(binaryOxygenRating, 2, 64)
	co2ScrubberRating, _ := strconv.ParseInt(binaryCo2Rating, 2, 64)

	return int(oxygenGeneratorRating * co2ScrubberRating)
}

// determineRating determines the final rating adhering to the respective business logic as
// defined on the AdventOfCode website.
func determineRating(bitCriteria BitCriteria, inputList []string, n int) string {
	var zeroes, ones []string

	if len(inputList) == 1 || n > len(inputList[0]) {
		return inputList[0]
	}

	for _, line := range inputList {
		if line[n] == '0' {
			zeroes = append(zeroes, line)
		} else if line[n] == '1' {
			ones = append(ones, line)
		}
	}

	// Oxygen takes the most common value, whilst CO2 takes the least common value
	if bitCriteria == OXYGEN {
		if len(zeroes) > len(ones) {
			return determineRating(bitCriteria, zeroes, n+1)
		}
		return determineRating(bitCriteria, ones, n+1)
	}

	if len(zeroes) > len(ones) {
		return determineRating(bitCriteria, ones, n+1)
	}
	return determineRating(bitCriteria, zeroes, n+1)
}

