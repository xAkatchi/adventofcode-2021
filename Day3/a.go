package main

import (
	"bufio"
	"strconv"
)

func runA(scn *bufio.Scanner) int {
	var gamma, epsilon = "", ""
	// Transpose the input so that we can just iterate through it.
	input := transposeScannerInput(scn)

	for _, data := range input {
		var countedZeroes, countedOnes = 0, 0

		for _, char := range data {
			if char == '0' {
				countedZeroes++
			} else if char == '1' {
				countedOnes++
			}
		}

		if countedOnes > countedZeroes {
			gamma += "1"
			epsilon += "0"
		} else {
			gamma += "0"
			epsilon += "1"
		}
	}

	gammaAsInt, _ := strconv.ParseInt(gamma, 2, 64)
	epsilonAsInt, _ := strconv.ParseInt(epsilon, 2, 64)

	return int(gammaAsInt * epsilonAsInt)
}

// transposeScannerInput transposes the input present in the given bufio.Scanner
// transposing in this context means that we 'rotate' the input 90 degrees, meaning
// that instead of getting a nxm slice, we're creating a mxn slice.
func transposeScannerInput(scn *bufio.Scanner) []string {
	var transposedInput []string

	for scn.Scan() {
		line := scn.Text()

		for i, char := range line {
			// If the key does not exist yet, we will have to create it.
			if len(transposedInput) < i + 1 {
				transposedInput = append(transposedInput, string(char))
			} else {
				transposedInput[i] += string(char)
			}
		}
	}

	return transposedInput
}