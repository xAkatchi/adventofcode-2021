package main

import (
	"bufio"
	"strconv"
)

func runB(scn *bufio.Scanner) int {
	var numberOfIncreases = 0

	scn.Scan()
	depthLast, _ := strconv.Atoi(scn.Text())

	scn.Scan()
	depthMiddle, _ := strconv.Atoi(scn.Text())

	scn.Scan()
	depthFirst, _ := strconv.Atoi(scn.Text())

	for scn.Scan() {
		newDepth, _ := strconv.Atoi(scn.Text())

		// We only have to compare the newest vs the oldest depth.
		// This because the other 2 depths will stay the same in the calculations
		// E.g.: A + B + C => X
		// V.s.: B + C + D => Y
		// The only way Y > X is if B > A, therefore no need to compare B and C.
		if newDepth > depthLast && newDepth > 0 {
			numberOfIncreases++
		}

		depthLast = depthMiddle
		depthMiddle = depthFirst
		depthFirst = newDepth
	}

	return numberOfIncreases
}
