package main

import (
	"AdventOfCode2021"
	"testing"
)

func TestRunA(t *testing.T) {
	tester := AdventOfCode2021.ChallengeTester{
		DayNr:      1,
		Part:       'A',
		TestMethod: runA,
	}

	tester.TestChallenge(t, []AdventOfCode2021.TestData{
		// It should count the amount of increases
		{Input: "5\n3\n7\n8\n9\n2", ExpectedOutput: 3},
		// If we go back to 0 it should still keep counting the following increases
		{Input: "3\n4\n0\n2", ExpectedOutput: 2},
		// Going negative (e.g. up in the sky) should be ignored
		{Input: "2\n-1\n-2\n0\n3", ExpectedOutput: 1},
		// Test case from the Advent of Code website
		{Input: "199\n200\n208\n210\n200\n207\n240\n269\n260\n263", ExpectedOutput: 7},
	})
}
