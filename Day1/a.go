package main

import (
	"bufio"
	"strconv"
)

func runA(scn *bufio.Scanner) int {
	scn.Scan()

	numberOfIncreases := 0
	prevDepth, _ := strconv.Atoi(scn.Text())

	for scn.Scan() {
		depth, _ := strconv.Atoi(scn.Text())

		// Don't count cases where we're going up in the sky
		if depth > prevDepth && depth > 0 {
			numberOfIncreases++
		}
		prevDepth = depth
	}

	return numberOfIncreases
}
