# AdventOfCode 2021

[![Coverage Report](https://gitlab.com/xakatchi/adventofcode-2021/badges/main/coverage.svg)](https://gitlab.com/xakatchi/adventofcode-2021/commits/main)

This repository contains my solutions (in Go) to the challenges provided by [Advent of Code](https://adventofcode.com/2021).