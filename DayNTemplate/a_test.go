package main

import (
	"AdventOfCode2021"
	"testing"
)

func TestRunA(t *testing.T) {
	tester := AdventOfCode2021.ChallengeTester{
		DayNr:      -1,
		Part:       'A',
		TestMethod: runA,
	}

	tester.TestChallenge(t, []AdventOfCode2021.TestData{
		// Test case from the Advent of Code website
		{Input: "", ExpectedOutput: -1},
	})
}
