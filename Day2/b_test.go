package main

import (
	"AdventOfCode2021"
	"testing"
)

func TestRunB(t *testing.T) {
	tester := AdventOfCode2021.ChallengeTester{
		DayNr:      2,
		Part:       'B',
		TestMethod: runB,
	}

	tester.TestChallenge(t, []AdventOfCode2021.TestData{
		// Test case from the Advent of Code website
		{Input: "forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2", ExpectedOutput: 900},
	})
}
