package main

import (
	"bufio"
	"strconv"
	"strings"
)

func runA(scn *bufio.Scanner) int {
	var horizontalPosition, depth = 0, 0

	for scn.Scan() {
		commands := strings.Split(scn.Text(), " ")

		// We need 2 entries, the first one being the command the second one being the value
		// if this is not the case, we will consider it an input error.
		if len(commands) != 2 {
			continue
		}

		command := commands[0]
		value, _ := strconv.Atoi(commands[1])

		switch command {
		case string(Forward):
			horizontalPosition += value
		case string(Up):
			depth -= value

			if depth < 0 {
				panic("We've unexpectedly reached land.")
			}
		case string(Down):
			depth += value
		}
	}

	return horizontalPosition * depth
}
