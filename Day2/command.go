package main

type Command string

const (
	Forward Command = "forward"
	Up      Command = "up"
	Down    Command = "down"
)
