package main

import (
	"AdventOfCode2021"
	"testing"
)

func TestRunB(t *testing.T) {
	tester := AdventOfCode2021.ChallengeTester{
		DayNr:      7,
		Part:       'B',
		TestMethod: runB,
	}

	tester.TestChallenge(t, []AdventOfCode2021.TestData{
		// Test case from the Advent of Code website
		{Input: "16,1,2,0,4,2,7,1,2,14", ExpectedOutput: 168},
	})
}
