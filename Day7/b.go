package main

import (
	"bufio"
	"math"
	"strconv"
	"strings"
)

func runB(scn *bufio.Scanner) int {
	scn.Scan()
	var positions []int

	for _, input := range strings.Split(scn.Text(), ",") {
		intInput, _ := strconv.Atoi(input)

		positions = append(positions, intInput)
	}

	// Whilst it is not exactly the average that we're trying to solve
	// (since that 'solves' for requiredSteps ^ 2 instead of the
	// requiredSteps(requiredSteps + 1) / 2)
	// it does work for this case, the only caveat is that we have to
	// check both the lower and upper bound of the average to find the lowest
	// value out of those.
	//
	// Whilst it is not the cleanest solution, it does get the job done.
	//
	// Read this thread for more information:
	// https://www.reddit.com/r/adventofcode/comments/rars4g/2021_day_7_why_do_these_values_work_spoilers/hnk7n2z/
	average := calcAverage(positions)
	flooredFuel := calculateFuelUsage(positions, math.Floor(average))
	ceiledFuel := calculateFuelUsage(positions, math.Ceil(average))

	return int(math.Min(flooredFuel, ceiledFuel))
}

func calcAverage(positions []int) float64 {
	sum := 0

	for _, value := range positions {
		sum += value
	}

	return float64(sum) / float64(len(positions))
}

func calculateFuelUsage(positions []int, average float64) float64 {
	fuel := 0.0

	for _, position := range positions {
		requiredSteps := int(math.Abs(float64(position) - average))

		fuel += float64((requiredSteps * (requiredSteps + 1)) / 2)
	}

	return fuel
}