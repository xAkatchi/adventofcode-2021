package main

import (
	"bufio"
	"math"
	"sort"
	"strconv"
	"strings"
)

func runA(scn *bufio.Scanner) int {
	scn.Scan()
	var positions []int

	for _, input := range strings.Split(scn.Text(), ",") {
		intInput, _ := strconv.Atoi(input)

		positions = append(positions, intInput)
	}

	median := calcMedian(positions)
	fuel := 0

	for _, position := range positions {
		requiredSteps := int(math.Abs(float64(position - median)))

		fuel += requiredSteps
	}

	return fuel
}

func calcMedian(positions []int) int {
	sort.Ints(positions)
	middle := len(positions) / 2

	if middle % 2 != 0 {
		return positions[middle]
	}

	return (positions[middle - 1] + positions[middle]) / 2
}