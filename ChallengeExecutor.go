package AdventOfCode2021

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

// List of colors obtained from: https://gist.github.com/ik5/d8ecde700972d4378d87
const (
	TitleColor  = "\033[1;31m%s\033[0m\n"
	TaskColor   = "\033[1;32m%s\033[0m\n"
	OutputColor = "\033[1;33m%d\033[0m\n"
)

type ChallengeExecutor struct {
	DayNumber int
	RunA      func(*bufio.Scanner) int
	RunB      func(*bufio.Scanner) int
}

func (c *ChallengeExecutor) Execute() {
	fmt.Printf(TitleColor, fmt.Sprintf("AdventOfCode 2021 [Day %d]", c.DayNumber))
	path, _ := os.Getwd()
	input, _ := os.Open(fmt.Sprintf("%s/Day%d/Input.txt", path, c.DayNumber))
	defer input.Close()
	scn := bufio.NewScanner(input)

	fmt.Printf(TaskColor, "[Executing] Part A:")
	fmt.Printf(OutputColor, c.RunA(scn))

	// Rewind the reader, so that we don't have to re-open the file for B
	_, _ = input.Seek(0, io.SeekStart)
	scn = bufio.NewScanner(input)

	fmt.Printf(TaskColor, "[Executing] Part B:")
	fmt.Printf(OutputColor, c.RunB(scn))
}
