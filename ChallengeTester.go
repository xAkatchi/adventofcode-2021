package AdventOfCode2021

import (
	"bufio"
	"bytes"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestData struct {
	Input          string
	ExpectedOutput int
}

type ChallengeTester struct {
	DayNr      int
	Part       byte
	TestMethod func(*bufio.Scanner) int
	TestData   []TestData
}

func (c *ChallengeTester) TestChallenge(t *testing.T, testData []TestData) {
	for i, testData := range testData {
		buf := bytes.NewBufferString(testData.Input)
		scanner := bufio.NewScanner(buf)
		output := c.TestMethod(scanner)

		assert.Equal(t, testData.ExpectedOutput, output,
			// i + 1 due to the 0 indexing of lists.
			fmt.Sprintf("[Day %d%c] The output of test %d did not match the expectations.", c.DayNr, c.Part, i+1),
		)
	}
}
